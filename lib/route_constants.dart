
const String splashRoute ='/Splash';
const String onboardingRoute ='/Onboarding';
const String myHomeRoute ='/MyHome';
const String loginRoute ='/Login';
const String signUpRoute ='/SignUp';
const String pagesRoute ='/Pages';
const String productRoute ='/Product';
const String detailsRoute ='/Details';
const String cartRoute ='/Cart';
const String consultationSummaryRoute ='/ConsultationSummary';
const String payOnPickupRoute ='/PayOnPickup';
const String healerPagesRoute ='/HealerPages';
const String healerRegisterRoute ='/HealerRegister';
const String healerRegisterSuccessRoute ='/HealerRegisterSuccess';
const String trackingRoute ='/Tracking';
const String schedulerRoute = "/Scheduler";
const String settingsRoute ='/Settings';
const String helpRoute ='/Help';
const String languagesRoute ='/Languages';
const String consultationSuccessRoute ='/OrderSuccess';
const String netCashRoute ='/NetCash';
const String profileRoute ='/Profile';
const String setPhotoScreenRoute ='/SetPhotoScreen';

const int notificationIndex = 0;
const int homeIndex = 1;
const int appointmentIndex = 2;

const int healerNotificationIndex = 2;
const int healerAppointmentIndex = 0;