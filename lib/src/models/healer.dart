import '../models/media.dart';

class Healer {
 late String id;
 late  String firebaseId;
 late  String name;
 late  Media? image;
 late  String rate;
 late  String address;
 late  String description;
 late  String practiceNumber;
 late  String mobile;
 late  String information;
 late  double adminCommission;
 late  double defaultTax;
 late  String latitude;
 late  String longitude;
 late String language;
 late  bool closed;
 late double distance;
 late double hourlyPrice;


  Healer.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      firebaseId = jsonMap['firebase_id'] ?? '';
      name = jsonMap['name'];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).isNotEmpty
          ? Media.fromJSON(jsonMap['media'][0])
          : Media();
      rate = jsonMap['rate'] ?? '0';
      adminCommission = jsonMap['admin_commission'] != null ? jsonMap['admin_commission'].toDouble() : 0.0;
      address = jsonMap['address']?? '';
      description = jsonMap['description']?? '';;
      practiceNumber = jsonMap['phone']?? '';;
      mobile = jsonMap['mobile']?? '';;
      defaultTax = jsonMap['default_tax'] != null ? jsonMap['default_tax'].toDouble() : 0.0;
      information = jsonMap['information']?? '';;
      latitude = jsonMap['latitude']?? '';
      longitude = jsonMap['longitude']?? '';
      language = jsonMap['language'] ?? '';
      closed = jsonMap['closed'] ?? false;
      distance = jsonMap['distance'] != null ? double.parse(jsonMap['distance'].toString()) : 0.0;
    } catch (e) {
      id = '';
      firebaseId = '';
      name = '';
      image =  Media();
      rate = '0';
      adminCommission = 0.0;
      address = '';
      description = '';
      practiceNumber = '';
      mobile = '';
      defaultTax = 0.0;
      information = '';
      latitude = '0';
      longitude = '0';
      language = '';
      closed = false;
      distance = 0.0;
      print(e);
    }
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firebase_id"] = firebaseId;
    map["name"] = name;
    if(image!=null){
      map["image"]= image?.toMap();
    }
    map["description"] = description;
    map["address"] = address;
    map["latitude"] = latitude;
    map["longitude"] = longitude;
    map["phone"] = practiceNumber;
    map["mobile"] = mobile;
    map["language"] = language;
    map["admin_commission"] = adminCommission;
    map["default_tax"] = defaultTax;
    map["closed"] = closed;
    map["information"] = information;

    return map;
  }
}
