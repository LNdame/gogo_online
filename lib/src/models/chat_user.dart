import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_user.g.dart';

@JsonSerializable()
class ChatUser {
  late final String id;
  //TODO add a uuid
  late final String username;
  late String? email;
  late String? imageUrl;
  late  String? role;
  late String? about;
  late  DateTime? aboutChangeDate;

  ChatUser({
     required this.id,
     required this.username,
  this.email,
    this.imageUrl,
    this.role,
    this.about,
    this.aboutChangeDate,
  });

  factory ChatUser.fromJson(Map<String, dynamic> data) {
    return _$ChatUserFromJson(data);
  }

  static Map<String, dynamic> toJson(ChatUser person) {
    return _$ChatUserToJson(person);
  }
}
