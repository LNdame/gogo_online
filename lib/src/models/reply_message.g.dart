// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reply_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReplyMessage _$ReplyMessageFromJson(Map<String, dynamic> json) => ReplyMessage(
      content: json['content'] as String?,
      replierId: json['replierId'] as String?,
      repliedToId: json['repliedToId'] as String?,
      type: $enumDecodeNullable(_$MessageTypeEnumMap, json['type']),
    );

Map<String, dynamic> _$ReplyMessageToJson(ReplyMessage instance) =>
    <String, dynamic>{
      'content': instance.content,
      'replierId': instance.replierId,
      'repliedToId': instance.repliedToId,
      'type': _$MessageTypeEnumMap[instance.type],
    };

const _$MessageTypeEnumMap = {
  MessageType.Text: 'Text',
  MessageType.Media: 'Media',
};
