class Faq {
 late String id;
 late String question;
 late String answer;

  Faq();

  Faq.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      question = jsonMap['question'] ?? '';
      answer = jsonMap['answer'] ?? '';
    } catch (e) {
      id = '';
      question = '';
      answer = '';
      print(e);
    }
  }
}
