import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/favorite.dart';
import '../repository/product_repository.dart';

class FavoriteController extends ControllerMVC {
  List<Favorite> favorites = <Favorite>[];
  late GlobalKey<ScaffoldState> scaffoldKey;

  static FavoriteController? _instance;
  factory FavoriteController() => _instance ??= FavoriteController._();


  FavoriteController._() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    listenForFavorites();
  }

  void listenForFavorites({String? message}) async {
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<Favorite> stream = await getFavorites();
    stream.listen((Favorite _favorite) {
      setState(() {
        favorites.add(_favorite);
      });
    }, onError: (a) {
      /* ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));*/
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(content: Text(message)));
      }
    });
  }

  Future<void> refreshFavorites() async {
    var stateCtx = state?.context;
    favorites.clear();
    listenForFavorites(message: AppLocalizations.of(stateCtx!)!.favorites_refreshed_successfuly);
  }
}
