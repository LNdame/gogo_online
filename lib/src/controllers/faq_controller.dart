import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/faq_category.dart';
import '../repository/faq_repository.dart';

class FaqController extends ControllerMVC {
  List<FaqCategory> faqs = <FaqCategory>[];
  late GlobalKey<ScaffoldState> scaffoldKey;

  FaqController() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    listenForFaqs();
  }

  void listenForFaqs({String? message}) async {
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<FaqCategory> stream = await getFaqCategories();
    stream.listen((FaqCategory _faq) {
      setState(() {
        faqs.add(_faq);
      });
    }, onError: (a) {
      print(a);
      /*  ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(state.context)!.verify_your_internet_connection),
      ));*/
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!)
            .showSnackBar(SnackBar(content: Text(message)));
      }
    });
  }

  Future<void> refreshFaqs() async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    faqs.clear();
    listenForFaqs(message: AppLocalizations.of(stateCtx!)!.faqsRefreshedSuccessfuly);
  }
}
