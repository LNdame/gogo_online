import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/notification.dart' as model;
import '../repository/notification_repository.dart';

class NotificationController extends ControllerMVC {
  List<model.Notification> notifications = <model.Notification>[];
 late GlobalKey<ScaffoldState> scaffoldKey;

  static NotificationController? _instance;
  factory NotificationController() => _instance ??= NotificationController._();

  NotificationController._() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    listenForNotifications();
  }

  void listenForNotifications({String? message}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<model.Notification> stream = await getNotifications();
    stream.listen((model.Notification _notification) {
      setState(() {
        notifications.add(_notification);
      });
    }, onError: (a) {
      print(a);
      /*ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));*/
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(content: Text(message)));
      }
    });
  }

  Future<void> refreshNotifications() async {
    var stateCtx = state?.context;
    notifications.clear();
    listenForNotifications(message: AppLocalizations.of(stateCtx!)!.notifications_refreshed_successfuly);
  }
}
